<?php

namespace Ensi\InternalMessenger;

class InternalMessengerClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\InternalMessenger\Api\AttachmentsApi',
        '\Ensi\InternalMessenger\Api\ChatsApi',
        '\Ensi\InternalMessenger\Api\MessagesApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\InternalMessenger\Dto\RequestBodyPagination',
        '\Ensi\InternalMessenger\Dto\SearchChatsResponseMeta',
        '\Ensi\InternalMessenger\Dto\SearchMessagesResponse',
        '\Ensi\InternalMessenger\Dto\Error',
        '\Ensi\InternalMessenger\Dto\RequestBodyOffsetPagination',
        '\Ensi\InternalMessenger\Dto\PaginationTypeEnum',
        '\Ensi\InternalMessenger\Dto\SearchChatsRequest',
        '\Ensi\InternalMessenger\Dto\ChatResponse',
        '\Ensi\InternalMessenger\Dto\File',
        '\Ensi\InternalMessenger\Dto\DeleteAttachmentRequest',
        '\Ensi\InternalMessenger\Dto\EmptyDataResponse',
        '\Ensi\InternalMessenger\Dto\ChatReadonlyProperties',
        '\Ensi\InternalMessenger\Dto\ChatFillableProperties',
        '\Ensi\InternalMessenger\Dto\UploadAttachmentResponseData',
        '\Ensi\InternalMessenger\Dto\ChatDirectionEnum',
        '\Ensi\InternalMessenger\Dto\SearchMessagesRequest',
        '\Ensi\InternalMessenger\Dto\SearchChatsFilter',
        '\Ensi\InternalMessenger\Dto\ResponseBodyPagination',
        '\Ensi\InternalMessenger\Dto\ChatIncludes',
        '\Ensi\InternalMessenger\Dto\ResponseBodyCursorPagination',
        '\Ensi\InternalMessenger\Dto\UploadAttachmentRequest',
        '\Ensi\InternalMessenger\Dto\ErrorResponse',
        '\Ensi\InternalMessenger\Dto\Chat',
        '\Ensi\InternalMessenger\Dto\UserTypeEnum',
        '\Ensi\InternalMessenger\Dto\ChatForPatch',
        '\Ensi\InternalMessenger\Dto\MessageReadonlyProperties',
        '\Ensi\InternalMessenger\Dto\SearchMessagesFilter',
        '\Ensi\InternalMessenger\Dto\ResponseBodyOffsetPagination',
        '\Ensi\InternalMessenger\Dto\RequestBodyCursorPagination',
        '\Ensi\InternalMessenger\Dto\ModelInterface',
        '\Ensi\InternalMessenger\Dto\SearchChatsResponse',
        '\Ensi\InternalMessenger\Dto\ChatForCreate',
        '\Ensi\InternalMessenger\Dto\Message',
        '\Ensi\InternalMessenger\Dto\MessageFillableProperties',
        '\Ensi\InternalMessenger\Dto\UploadAttachmentResponse',
    ];

    /** @var string */
    public static $configuration = '\Ensi\InternalMessenger\Configuration';
}
