<?php
/**
 * MessageTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\InternalMessenger
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. Internal messenger. Внутренние чаты
 *
 * Управление внутренними чатами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\InternalMessenger;

use PHPUnit\Framework\TestCase;

/**
 * MessageTest Class Doc Comment
 *
 * @category    Class
 * @description Message
 * @package     Ensi\InternalMessenger
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class MessageTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Message"
     */
    public function testMessage()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }

    /**
     * Test attribute "user_id"
     */
    public function testPropertyUserId()
    {
    }

    /**
     * Test attribute "user_type"
     */
    public function testPropertyUserType()
    {
    }

    /**
     * Test attribute "chat_id"
     */
    public function testPropertyChatId()
    {
    }

    /**
     * Test attribute "text"
     */
    public function testPropertyText()
    {
    }

    /**
     * Test attribute "files"
     */
    public function testPropertyFiles()
    {
    }
}
