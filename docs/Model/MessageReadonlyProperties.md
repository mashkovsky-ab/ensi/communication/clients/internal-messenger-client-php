# # MessageReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор сообщения | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания сообщения | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления сообщения | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


