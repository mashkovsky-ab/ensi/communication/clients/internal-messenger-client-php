# # SearchMessagesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\InternalMessenger\Dto\Message[]**](Message.md) |  | 
**meta** | [**\Ensi\InternalMessenger\Dto\SearchChatsResponseMeta**](SearchChatsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


