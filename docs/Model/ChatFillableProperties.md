# # ChatFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Идентификатор пользователя | [optional] 
**user_type** | **int** | Тип пользователя | [optional] 
**direction** | **int** | Направление чата | [optional] 
**type_id** | **int** | Тип чата | [optional] 
**theme** | **string** | Тема чата | [optional] 
**muted** | **bool** | Флаг чата без ответов | [optional] 
**unread_user** | **bool** | Флаг непрочтении юзером | [optional] 
**unread_admin** | **bool** | Флаг непрочтении админом | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


