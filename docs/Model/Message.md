# # Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор сообщения | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания сообщения | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления сообщения | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 
**user_type** | **int** | Тип пользователя | [optional] 
**chat_id** | **int** | Идентификатор чата | [optional] 
**text** | **string** | Содержание сообщения | [optional] 
**files** | [**\Ensi\InternalMessenger\Dto\File[]**](File.md) | Прикрепленные файлы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


