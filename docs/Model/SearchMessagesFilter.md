# # SearchMessagesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор сообщения | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 
**user_type** | **int** | Тип пользователя | [optional] 
**chat_id** | **int** | Идентификатор чата | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


