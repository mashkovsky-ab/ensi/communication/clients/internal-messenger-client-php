# # SearchChatsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор чата | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 
**type_id** | **int** | Тип чата | [optional] 
**user_type** | **int** | Тип пользователя | [optional] 
**unread_admin** | **bool** | Флаг непрочтении админом | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


