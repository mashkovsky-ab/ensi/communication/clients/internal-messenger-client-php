# Ensi\InternalMessenger\AttachmentsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAttachment**](AttachmentsApi.md#createAttachment) | **POST** /attachments | Загрузка файла
[**deleteAttachment**](AttachmentsApi.md#deleteAttachment) | **DELETE** /attachments | Удаление файлов



## createAttachment

> \Ensi\InternalMessenger\Dto\UploadAttachmentResponse createAttachment($file, $name)

Загрузка файла

Загрузка файла

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\AttachmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл
$name = 'name_example'; // string | Имя файла (опционально)

try {
    $result = $apiInstance->createAttachment($file, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsApi->createAttachment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]
 **name** | **string**| Имя файла (опционально) | [optional]

### Return type

[**\Ensi\InternalMessenger\Dto\UploadAttachmentResponse**](../Model/UploadAttachmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteAttachment

> \Ensi\InternalMessenger\Dto\EmptyDataResponse deleteAttachment($delete_attachment_request)

Удаление файлов

Удаление файлов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\AttachmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_attachment_request = new \Ensi\InternalMessenger\Dto\DeleteAttachmentRequest(); // \Ensi\InternalMessenger\Dto\DeleteAttachmentRequest | 

try {
    $result = $apiInstance->deleteAttachment($delete_attachment_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsApi->deleteAttachment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_attachment_request** | [**\Ensi\InternalMessenger\Dto\DeleteAttachmentRequest**](../Model/DeleteAttachmentRequest.md)|  | [optional]

### Return type

[**\Ensi\InternalMessenger\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

