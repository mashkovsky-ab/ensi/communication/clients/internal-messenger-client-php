# Ensi\InternalMessenger\MessagesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchMessages**](MessagesApi.md#searchMessages) | **POST** /messages:search | Получить список сообщений, удовлетворяющих условиям



## searchMessages

> \Ensi\InternalMessenger\Dto\SearchMessagesResponse searchMessages($search_messages_request)

Получить список сообщений, удовлетворяющих условиям

Поиск объектов типа Message

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\MessagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_messages_request = new \Ensi\InternalMessenger\Dto\SearchMessagesRequest(); // \Ensi\InternalMessenger\Dto\SearchMessagesRequest | 

try {
    $result = $apiInstance->searchMessages($search_messages_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MessagesApi->searchMessages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_messages_request** | [**\Ensi\InternalMessenger\Dto\SearchMessagesRequest**](../Model/SearchMessagesRequest.md)|  |

### Return type

[**\Ensi\InternalMessenger\Dto\SearchMessagesResponse**](../Model/SearchMessagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

